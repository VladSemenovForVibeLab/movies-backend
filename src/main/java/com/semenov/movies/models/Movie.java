package com.semenov.movies.models;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "movies")
@Schema(description = "Movie entity")
public class Movie {
    @Id
    @Schema(description = "id", example = "{\n" +
            "        \"timestamp\": 1694680476,\n" +
            "        \"date\": \"2023-09-14T08:34:36.000+00:00\"\n" +
            "    }")
    private ObjectId id;

    @Schema(description = "ImdbId", example = "tt3915174")
    private String imdbId;

    @Schema(description = "title", example = "Puss in Boots: The Last Wish")
    private String title;

    @Schema(description = "releaseDate",example = "2022-12-21")
    private String releaseDate;

    @Schema(description = "trailerLink",example = "https://www.youtube.com/watch?v=tHb7WlgyaUc")
    private String trailerLink;

    @Schema(description = "poster",example = "https://image.tmdb.org/t/p/w500/1NqwE6LP9IEdOZ57NCT51ftHtWT.jpg")
    private String poster;

    @Schema(description = "genres",example = "[\n" +
            "        \"Animation\",\n" +
            "        \"Action\",\n" +
            "        \"Adventure\",\n" +
            "        \"Comedy\",\n" +
            "        \"Family\"\n" +
            "    ]")
    private List<String> genres;

    @Schema(description = "backdrops",example = "[\n" +
            "        \"https://image.tmdb.org/t/p/original/r9PkFnRUIthgBp2JZZzD380MWZy.jpg\",\n" +
            "        \"https://image.tmdb.org/t/p/original/faXT8V80JRhnArTAeYXz0Eutpv9.jpg\",\n" +
            "        \"https://image.tmdb.org/t/p/original/pdrlEaknhta2wvE2dcD8XDEbAI4.jpg\",\n" +
            "        \"https://image.tmdb.org/t/p/original/tGwO4xcBjhXC0p5qlkw37TrH6S6.jpg\",\n" +
            "        \"https://image.tmdb.org/t/p/original/cP8YNG3XUeBmO8Jk7Skzq3vwHy1.jpg\",\n" +
            "        \"https://image.tmdb.org/t/p/original/qLE8yuieTDN93WNJRmFSAEJChOg.jpg\",\n" +
            "        \"https://image.tmdb.org/t/p/original/vNuHqmOJRQXY0PBd887DklSDlBP.jpg\",\n" +
            "        \"https://image.tmdb.org/t/p/original/uUCc62M0I3lpZy0SiydbBmUIpNi.jpg\",\n" +
            "        \"https://image.tmdb.org/t/p/original/2wPJIFrBhzzAP8oHDOlShMkERH6.jpg\",\n" +
            "        \"https://image.tmdb.org/t/p/original/fnfirCEDIkxZnQEtEMMSgllm0KZ.jpg\"\n" +
            "    ]")
    private List<String> backdrops;

    @Schema(description = "reviewIds",example = "[\n" +
            "        {\n" +
            "            \"id\": {\n" +
            "                \"timestamp\": 1694685453,\n" +
            "                \"date\": \"2023-09-14T09:57:33.000+00:00\"\n" +
            "            },\n" +
            "            \"body\": \"Классное кино\"\n" +
            "        }\n" +
            "    ]")
    @DocumentReference
    private List<Review> reviewIds;
}
