package com.semenov.movies.models;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "reviews")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Review entity")
public class Review {
    @Id
    @Schema(description = "id", example = "{\n" +
            "                \"timestamp\": 1694685453,\n" +
            "                \"date\": \"2023-09-14T09:57:33.000+00:00\"\n" +
            "            }")
    private ObjectId id;
    @Schema(description = "body",example = "Классное кино")
    private String body;

    public Review(String body) {
        this.body = body;
    }
}
